# README #

This is an event log generator for BPMN collaborations.

## Usage

Make sure to have installed Java in your system. Then run the jar file from terminal with 2 parameters:

* path to BPMN model file
* number of traces

For instance:

```bash
java -jar CollaborativeLogGenerator.jar mymodel.bpmn 1000
```

The output XES files will be produced in the current directory.
